/*
5. Geometria
Zdefiniować klasę Figura w geometrii euklidesowej, czyli operującej na punkcie i odcinku.
Zdefiniować możliwość obliczania pola figury(p), obwodu(o), przesunięcia o ustaloną odległość(t),
symetrii względem osi x(sx) i osi y(sy). Ogranicz się do figur: kwadrat(k), koło(o), trójkąt(t).
Kwadrat i trójkąt zapisywane są jako lista kolejnych punktów (x y), natomiast koło jako środek (x,
y) oraz promień (r)

Przykład:

Wejście:
p k 0 0 5 0 5 5 0 5
Wyjście:
25

Przykład:

Wejście:
o t 0 0 3 0 0 4

Wyjście:
12.0

Przykład:

Wejście:
sx o 5 0 3

Wyjście:
o 5 0 3

*/
#include <iostream>
#include <cmath>
#include <vector>

struct Point
{
    double x;
    double y;
};

struct Vector
{
    Point origin;
    Point end;
    double length;
};

class Shape
{
    public:
	virtual double perimeter() = 0;
	virtual double area() = 0;
    virtual void translateByVector(Point t) = 0;
    virtual void symX() = 0;
    virtual void symY() = 0;

    virtual void print(std::ostream& output) = 0;
	

	virtual ~Shape() {}
};



class Circle: public Shape 
{
    public:
        Circle() {};
        Circle(double ax, double ay, double r) {center.x = ax, center.y = ay, radius = r;}
        
        double perimeter()
        {
            return 2 * 3.14 * radius;
        }
        
        double area() 
        {
            return 3.14 * radius * radius;
        }
    
        void translateByVector(Point t)
        {
            center.x += t.x;
            center.y += t.y; 
        }

        void symX()
        {
            if(center.y != 0)
                center.y *= -1;
        }

        void symY()
        {
            if(center.x != 0)
                center.x *= -1;
        }

        void print(std::ostream& out)
        {
            out << "o " << center.x << " " << center.y << " " << radius;
        }

    private:
        Point center;
        double radius;
};

class Triangle: public Shape
{
    public:
        Triangle() {};
        Triangle(double ax, double ay, double bx, double by, double cx, double cy): vertices(3), edges(3)
        {vertices[0].x = ax, vertices[0].y = ay, vertices[1].x = bx, vertices[1].y = by, vertices[2].x = cx, vertices[2].y = cy;
        for(int i = 0; i < vertices.size(); ++i)
            edges[i].length = sqrt(((vertices[i+1].x - vertices[i].x) * (vertices[i+1].x - vertices[i].x)) + ((vertices[i+1].y - vertices[i].y) * (vertices[i+1].y - vertices[i].y)));
        }

        double perimeter()
        {
            return edges[0].length + edges[1].length + edges[2].length;
        }

        double area()
        {
            double p = perimeter() / 2;
            return sqrt(((p - edges[0].length) * (p - edges[1].length) * (p - edges[2].length)) * p);
        }        

        void translateByVector(Point t)
        {
            for(int i = 0; i < vertices.size(); ++i)
            {
                vertices[i].x += t.x;
                vertices[i].y += t.y;
            }
        }

        void symX()
        {
            for(int i = 0; i < vertices.size(); ++i)
                if(vertices[i].y != 0)
                    vertices[i].y *= -1;
        }

        void symY()
        {
            for(int i = 0; i < vertices.size(); ++i)
                if(vertices[i].x != 0)
                    vertices[i].x *= -1;
        }

        void print(std::ostream& out)
        {
            out << "t ";
             for(int i = 0; i < vertices.size(); ++i)
                out << vertices[i].x << " " << vertices[i].y << " ";
        }

    private:
        std::vector<Point> vertices;
        std::vector<Vector> edges;
};

class Square: public Shape
{
    public:
        Square() {};
        Square(double ax, double ay, double bx, double by, double cx, double cy, double dx, double dy): vertices(4)
        {vertices[0].x = ax, vertices[0].y = ay, vertices[1].x = bx, vertices[1].y = by, vertices[2].x = cx, vertices[2].y = cy, vertices[3].x = dx, vertices[3].y = dy;}
        
        double perimeter()
        {
            double edge1 = sqrt(((vertices[1].x - vertices[0].x) * (vertices[1].x - vertices[0].x)) + ((vertices[1].y - vertices[0].y) * (vertices[1].y - vertices[0].y)));
            double edge2 = sqrt(((vertices[2].x - vertices[1].x) * (vertices[2].x - vertices[1].x)) + ((vertices[2].y - vertices[1].y) * (vertices[2].y - vertices[1].y)));
            if(edge1 > edge2)
                return 4 * edge2;
            else 
                return 4 * edge1;
            //ze wzoru pitagorasa odleglosc miedzy dwoma kolejnymi punktami
        }

        double area()
        {  
            double a1 = (((vertices[1].x - vertices[0].x) * (vertices[1].x - vertices[0].x)) + ((vertices[1].y - vertices[0].y) * (vertices[1].y - vertices[0].y)));
            double a2 = (((vertices[2].x - vertices[1].x) * (vertices[2].x - vertices[1].x)) + ((vertices[2].y - vertices[1].y) * (vertices[2].y - vertices[1].y)));
            if(a1 > a2)
                return a2;
            else 
                return a1;
        }

        void translateByVector(Point t)
        {
            for(int i = 0; i < vertices.size(); ++i)
            {
                vertices[i].x += t.x;
                vertices[i].y += t.y;
            }
        }

        void symX()
        {
            for(int i = 0; i < vertices.size(); ++i)
                if(vertices[i].y != 0)
                    vertices[i].y *= -1;
        }

        void symY()
        {
            for(int i = 0; i < vertices.size(); ++i)
                if(vertices[i].x != 0)
                    vertices[i].x *= -1;
        }

        void print(std::ostream& out)
        {
            out << "k ";
             for(int i = 0; i < vertices.size(); ++i)
                out << vertices[i].x << " " << vertices[i].y << " ";
        }


    private:
        std::vector<Point> vertices;
};

std::ostream& operator<<(std::ostream& out, Shape* sh) 
{
    sh->print(out);
    return out;
}
/*
shape = o - kolo, k - kwadrat, t -trojkat
operationType = p - pole, o - obwod, t - przesuniecie o wektor, sx - symetria OX, sy - symetria OY
*/

int main() 
{
    Shape* shape;
    
    char sh, op;
    std::string in;

    std::cin >> in >> sh;
    
    if(in.length() == 2)
        op = in[1];
    else
        op = in[0];

    switch(sh)
    {
        case 'o':
        {
            double x, y, r;
            std::cin >> x >> y >> r;
            shape = new Circle(x, y, r);
            break;
        }
        case 't':
        {
            double ax, ay, bx, by, cx, cy;
            std::cin >> ax >> ay >> bx >> by >> cx >> cy;
            shape = new Triangle(ax, ay, bx, by, cx, cy);
            break;
        }
        case 'k':
        {
            double ax, ay, bx, by, cx, cy, dx, dy;
            std::cin >> ax >> ay >> bx >> by >> cx >> cy >> dx >> dy;
            shape = new Square(ax, ay, bx, by, cx, cy, dx, dy);
            break;
        }
    }
    
    switch(op)
    {
        case 'p':
        {
            std::cout << shape->area();
            break;
        }
        case 'o':
        {
            
            std::cout << shape->perimeter();
            break;
        }
        case 't':
        {
            Point tr;
            std::cin >> tr.x >> tr.y;
            shape->translateByVector(tr);
            std::cout << shape;
            break;
        }
        case 'x':
        {
            shape->symX();
            std::cout << shape;
            break;
        }
        case 'y':
        {
            shape->symY();
            std::cout << shape;
            break;
        }
    }

    delete shape;
}